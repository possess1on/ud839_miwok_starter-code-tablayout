package com.example.android.miwok;

public class Word {
    private int songG;
    /** Default translation for the word */
    private String mDefaultTranslation;
    /** Miwok translation for the word */
    private String mMiwokTranslation;
    private int mImageRes = NO_IMAGE;
    private static final int NO_IMAGE = -1;

    /**
     * Create a new Word object.
     *
     * @param defaultTranslation is the word in a language that the user is already familiar with
     *                           (such as English)
     * @param miwokTranslation is the word in the Miwok language
     */
//    public Word(String defaultTranslation, String miwokTranslation) {
//        mDefaultTranslation = defaultTranslation;
//        mMiwokTranslation = miwokTranslation;
//    }
    public Word(String defaultTranslation, String miwokTranslation, int song) {
        mDefaultTranslation = defaultTranslation;
        mMiwokTranslation = miwokTranslation;
        songG = song;
    }
//    public Word(String defaultTranslation, String miwokTranslation, int imageRes) {
//        mDefaultTranslation = defaultTranslation;
//        mMiwokTranslation = miwokTranslation;
//        mImageRes = imageRes;
//    }

    public Word(String defaultTranslation, String miwokTranslation, int imageRes, int song) {
        mDefaultTranslation = defaultTranslation;
        mMiwokTranslation = miwokTranslation;
        mImageRes = imageRes;
        songG = song;
    }

    /**
     * Get the default translation of the word.
     */
    public String getDefaultTranslation() {
        return mDefaultTranslation;
    }
    /**
     * Get the Miwok translation of the word.
     */
    public String getMiwokTranslation() {
        return mMiwokTranslation;
    }
    public int getImageRes(){
        return  mImageRes;
    }
    public int getSong(){
        return songG;
    }
    public boolean hasImage (){
        return mImageRes != NO_IMAGE;
    }

}
